import ast
#Entrada: Nombre del archivo a cargar
#Salida: String contenido del archivo
#Que hace: carga el contenido de un archivo a un string
def leerArchivo(nombreArchivo):
  file = open (nombreArchivo,'r')
  contenido = file.read()
  file.close()
  return contenido

################################################################################
#Entrada: Nombre del archivo a cargar donde esta en diccionario
#Salida: diccionario 
#Que hace: carga el  diccionario contendio en un archivo a un variable
def cargarDiccionario(nombreArchivoDiccionario):
  buffer = leerArchivo(nombreArchivoDiccionario)
  buffer = buffer.replace("\n", "")
  dict = ast.literal_eval(buffer)
  return dict

################################################################################
#Entrada: Nombre del archivo y el contenido a escribir en el archivo
#Salida: ninguna
#Que hace: se crea un archivo con el contenido explicito ingresado
def escribirArchivo(nombreArchivo, contenido):
  file = open (nombreArchivo,'w')
  file.write(contenido)
  file.close()

################################################################################
#Entrada: Ninguna
#Salida: Ninguna
#Que hace: Corre el programa principal
#def Main():
#  dict = cargarDiccionario('diccionario.txt')
#  print(dict["a"])
#  escribirArchivo("pruebaEscritura.txt", "Prueba de escritura en archivo")
  
###### Main ####################################################################
#Main()
